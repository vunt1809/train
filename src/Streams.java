import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams {
	public static void main(String[] args) {

		// Viết hoa tất cả các phần tử trong list
		List<String> alphabets = Arrays.asList(new String[] { "a", "b", "c", "d" });
		for (String letter : alphabets) {
			System.out.println(letter.toUpperCase());
		}

		// Lọc ra các số chia hết cho 2
		List<Integer> numbers = Arrays.asList(7, 2, 5, 4, 2, 1);
		long count = numbers.stream().filter(num -> num % 2 == 0).count();
		System.out.println("Số lượng số chia hết cho 2 : " + count);

		// Tạo stream từ collection
		List<String> items = new ArrayList<>();
		items.add("Java");
		items.add("C#");
		items.add("C++");
		items.add("PHP");
		items.add("Javascript");
		items.stream().forEach(item -> System.out.println(item));

		// Tạo steam từ 1 mảng
		String[] languages = { "Java", "C#", "C++", "PHP", "Javascript" };
		// Get Stream using the Arrays.stream
		Stream<String> testStream1 = Arrays.stream(languages);
		testStream1.forEach(x -> System.out.println(x));
		// Get Stream using the Stream.of
		Stream<String> testStream2 = Stream.of(languages);
		testStream2.forEach(x -> System.out.println(x));

		// Tạo Stream sang các cấu trúc dữ liệu khác
		//// Collection
		Stream<String> stream = Stream.of("Java", "C#", "C++", "PHP", "Javascript");
		List<String> languages2 = stream.collect(Collectors.toList());
		System.out.println(languages2);

		// List Name
		List<String> memberNames = new ArrayList<>();
		memberNames.add("An");
		memberNames.add("Song");
		memberNames.add("Anh");
		memberNames.add("Nam");
		memberNames.add("Sinh");
		memberNames.add("Vu");
		memberNames.add("Quynh");
		memberNames.add("Linh");
		memberNames.add("ABC123");
		memberNames.add("Test12");
		// Steam filter name với đầu tiên chữ S sử dụng forEach
		memberNames.stream().filter((s) -> s.startsWith("S")).forEach(System.out::println);
		
		// Sắp xếp tên stream.sorted
		memberNames.stream().sorted().map(String::toUpperCase).forEach(System.out::println);
		
		// Stream count tên chữ S đầu
		long totalMatched = memberNames.stream().filter((s) -> s.startsWith("S")).count();
		System.out.println(totalMatched);
		
		// Stream match
		// 1 trong tat ca cac ten bat dau bang chu A
		boolean matchedResult = memberNames.stream().anyMatch((s) -> s.startsWith("A"));
		System.out.println(matchedResult);
		
		// Tat ca cac ten bat dau bang chu A
		matchedResult = memberNames.stream().allMatch((s) -> s.startsWith("A"));
		System.out.println(matchedResult);
		
		// Khong ten nao bat dau bang chu A
		matchedResult = memberNames.stream().noneMatch((s) -> s.startsWith("A"));
		System.out.println(matchedResult);
		// 
		matchedResult = memberNames.stream().noneMatch((s) -> s.startsWith("X"));
		System.out.println(matchedResult);
		
		// Stream find first
		String firstMatchedName = memberNames.stream()
                .filter((s) -> s.startsWith("S"))
                .findFirst().get();
		System.out.println(firstMatchedName);
		// Find name giống
		String Findname = memberNames.stream()
				.filter((s)->s.startsWith("ABC"))
				.findFirst().get();
		System.out.println(Findname);
		// Find name không giống
		String Findname2 = memberNames.stream()
				.filter((s)->s.startsWith("ABD"))
				.findFirst().get();
		System.out.println(Findname);
		// 
	}
}
