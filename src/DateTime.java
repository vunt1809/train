import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

public class DateTime {
	public static void main(String[] args) {
		// Sử dụng LocalDate Lấy ngày hiện tại
		LocalDate today = LocalDate.now();
		System.out.println("Today : " + today);
		
		// Sử dụng LocalDate Ngày này tuần sau
		LocalDate nextWeek = today.plus(1, ChronoUnit.WEEKS);
        System.out.println("Next week: " + nextWeek);
        
        // Sử dụng LocalTime lấy giờ hiện tại 
        LocalTime time = LocalTime.now();
        System.out.println("Current Time=" + time);
        
        // Sử dụng LocalTime Lấy giờ tại 1 thành phố HCM
        LocalTime timeHCM = LocalTime.now(ZoneId.of("Asia/Ho_Chi_Minh"));
        System.out.println("Time in HCM City = " + timeHCM);
        
        //Sử dụng LocalDateTime
        LocalDateTime today2 = LocalDateTime.now();
        today2 = LocalDateTime.of(LocalDate.now(), LocalTime.now());
        System.out.println("Current DateTime = " + today2);
        
        //Timestamp 
        Instant now = Instant.now();
        System.out.println("Current Timestamp = " + now);
        // Istant
        Instant instant = Instant.now();
        System.out.println("Instant : "+instant.toString());
        //Duration
        Duration duration = Duration.ofMillis(5000);
        System.out.println("Duration : "+duration.toString()); 
        
        //Period
        LocalDate firstDate = LocalDate.now();
        LocalDate secondDate = LocalDate.of(2020, 12, 20);
        System.out.println("firstDate: " + firstDate); 
        System.out.println("secondDate: " + secondDate);
        Period period = Period.between(firstDate, secondDate);
        System.out.println("period: " + period);
        //Priod2
        period = Period.between(LocalDate.now(),
                LocalDate.now().plusDays(60));
        System.out.println(period.toString());
        
        
	}
}
