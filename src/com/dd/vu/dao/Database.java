package com.dd.vu.dao;

import java.util.ArrayList;


public class Database {
	public ArrayList<Object> productTable;
	public ArrayList<Object> categoryTable;
	public ArrayList<Object> accessoryTable;
	public Database instants;

	public Database() {
		this.productTable = new ArrayList<Object>();
		this.categoryTable = new ArrayList<Object>();
		this.accessoryTable = new ArrayList<Object>();
	}

	
	public Integer insertTable(String name, Object row) {
		int i = 0;
		if (name.equals("product")) {
			this.productTable.add(row);
			i = 1;
		}
		if (name.equals("category")) {
			this.categoryTable.add(row);
			i = 1;
		}
		if (name.equals("accessory")) {
			this.accessoryTable.add(row);
			i = 1;
		}
		return i;
	}

	public ArrayList<Object> selectTable(String name) {
		if (name.equals("product")) {
			return this.productTable;
		}
		if (name.equals("category")) {
			return this.categoryTable;
		}
		if (name.equals("accessory")) {
			return this.accessoryTable;
		}
		return null;
	}

	public Integer updateTable(String name, Object row) {
		int i = 0;
		if (name.equals("product")) {
			this.productTable.set(0, row);
			i = 1;
		}
		if (name.equals("category")) {
			this.categoryTable.set(0,row);
			i = 1;
		}
		if (name.equals("accessory")) {
			this.accessoryTable.set(0,row);
			i=-1;
		}
		return i;
	}

	public boolean deleteTable(String name, Object row) {
		boolean check = false;
		if (name.equals("product")) {
			check = this.productTable.remove(row);
		}
		if (name.equals("category")) {
			check = this.categoryTable.remove(row);
		}
		if (name.equals("accessory")) {
			check = this.accessoryTable.remove(row);
		}
		return check;
	}

	public void truncateTable(String name) {
		if (name.equals("product")) {
			this.productTable.clear();
			System.out.println("Truncate product");
		}
		if (name.equals("category")) {
			this.categoryTable.clear();
			System.out.println("Truncate category");
		}
		if (name.equals("accessory")) {
			this.accessoryTable.clear();
			System.out.println("Truncate accessory");
		}
	}

	public ArrayList<Object> getProductTable() {
		return productTable;
	}

	public void setProductTable(ArrayList<Object> productTable) {
		this.productTable = productTable;
	}

	public ArrayList<Object> getCategoryTable() {
		return categoryTable;
	}

	public void setCategoryTable(ArrayList<Object> categoryTable) {
		this.categoryTable = categoryTable;
	}

	public ArrayList<Object> getAccessoryTable() {
		return accessoryTable;
	}

	public void setAccessoryTable(ArrayList<Object> accessoryTable) {
		this.accessoryTable = accessoryTable;
	}

	public Database getInstants() {
		return instants;
	}

	public void setInstants(Database instants) {
		this.instants = instants;
	}
	
	

}
