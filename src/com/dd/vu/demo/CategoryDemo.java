package com.dd.vu.demo;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.entity.Category;


public class CategoryDemo {
	
	public static void main(String[] args) {
		List<Category> listCategory = createCategoryTest();
		System.out.println(listCategory);
		Category category = printCategory(listCategory, 1);
		System.out.println(category);
	}
	
	public static List<Category> createCategoryTest(){
		List<Category> category = new ArrayList<Category>();
		try {
			category.add(new Category(1,"Giat Khat"));
			category.add(new Category(2,"Loai co ga"));
			category.add(new Category(3,"Loai Sua"));
			category.add(new Category(4,"Loai co con"));
			return category;
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return category;
	}
	
	private static Category printCategory(List<Category> lisCategory, int id) {
		Category result = null;
		for (Category temp : lisCategory) {
			if (id == temp.getId()) {
				result = temp;
			}
		}
		return result;
	}
}

