package com.dd.vu.demo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.dd.vu.entity.Product;

public class ProductDemo {
	public static void main(String[] args) {
		List<Product> listProducts = createProductTest();
		System.out.println(listProducts);
		Product product = printProduct(listProducts, 1);
		System.out.println(product);
	}
	
	public static List<Product> createProductTest() {
		List<Product> product = new ArrayList<Product>();
		LocalDate hientai = LocalDate.now();
		LocalDate tuansau = hientai.plus(1, ChronoUnit.WEEKS);
		LocalDate tuantruoc = hientai.minus(1, ChronoUnit.WEEKS);
		try {
			product.add(new Product(1, "Ca  phe", 2, hientai, 10, true));
			product.add(new Product(2, "Ca  phe den", 2, tuansau, 4, true));
			product.add(new Product(3, "Ca  phe sua", 2, tuansau, 2, false));
			product.add(new Product(4, "Coca cola", 3, tuansau, 0, true));
			product.add(new Product(5, "Pepsi", 3, hientai, 2, false));
			product.add(new Product(6, "7up", 3, hientai, 3, true));
			product.add(new Product(7, "Banh ngot", 4, tuantruoc, 4, false));
			product.add(new Product(8, "Banh kem", 4, tuantruoc, 8, true));
			product.add(new Product(9, "Tra sua", 1, tuantruoc, 0, false));
			product.add(new Product(10, "Kem", 2, hientai, 0, true));
			return product;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return product;
	}

	private static Product printProduct(List<Product> listProducts, int id) {
		Product result = null;
		for (Product temp : listProducts) {
			if (id == temp.getId()) {
				result = temp;
			}
		}
		return result;
	}
}
