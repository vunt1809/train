package com.dd.vu.demo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.dd.vu.dao.Database;
import com.dd.vu.entity.Accessory;
import com.dd.vu.entity.Category;
import com.dd.vu.entity.Product;

public class DatabaseDemo {
	static Database dataBase = new Database();

	public static void main(String[] args) {

		selectTableTest();
		updateTableTest();
		deleteTableTest();
		truncateTableTest();
		initDatabase();
		printTableTest();
	}

	private static void insertTableTest() {
		Product product = new Product(1, "Product1");
		Product product2 = new Product(2, "Product2");
		dataBase.insertTable("product", product);
		dataBase.insertTable("product", product2);
		Category category = new Category(1, "Category1");
		Category category2 = new Category(2, "Category2");
		dataBase.insertTable("category", category);
		dataBase.insertTable("category", category2);
		Accessory accessory = new Accessory(1, "Accessory");
		dataBase.insertTable("accessory", accessory);
	}

	private static void selectTableTest() {
		insertTableTest();
		ArrayList<Object> products = dataBase.selectTable("product");
		System.out.println("Select :" + products);
		ArrayList<Object> categorys = dataBase.selectTable("category");
		System.out.println("Select :" + categorys);
		ArrayList<Object> accessorys = dataBase.selectTable("accessory");
		System.out.println("Select :" + accessorys);
	}

	private static void updateTableTest() {
		Database dataBase = new Database();
		Object product = new Category(1, "Abc");
		dataBase.updateTable("category", product);
	}

	private static boolean deleteTableTest() {
		ArrayList<Object> objCategory = dataBase.selectTable("category");
		Object category = objCategory.get(0);
		Boolean isDelete = dataBase.deleteTable("category", category);
		System.out.println("Delete : " + category + " Trang Thai : " + isDelete);
		return isDelete;
	}

	private static void truncateTableTest() {
		dataBase.truncateTable("product");
	}

	private static void initDatabase() {
		// Tao list Product
		ArrayList<Product> product = new ArrayList<Product>();
		LocalDate hientai = LocalDate.now();
		LocalDate tuansau = hientai.plus(1, ChronoUnit.WEEKS);
		LocalDate tuantruoc = hientai.minus(1, ChronoUnit.WEEKS);
		try {
			product.add(new Product(1, "Ca  phe", 2, hientai, 10, true));
			product.add(new Product(2, "Ca  phe den", 2, tuansau, 4, true));
			product.add(new Product(3, "Ca  phe sua", 2, tuansau, 2, false));
			product.add(new Product(4, "Coca cola", 3, tuansau, 0, true));
			product.add(new Product(5, "Pepsi", 3, hientai, 2, false));
			product.add(new Product(6, "7up", 3, hientai, 3, true));
			product.add(new Product(7, "Banh ngot", 4, tuantruoc, 4, false));
			product.add(new Product(8, "Banh kem", 4, tuantruoc, 8, true));
			product.add(new Product(9, "Tra sua", 1, tuantruoc, 0, false));
			product.add(new Product(10, "Kem", 2, hientai, 0, true));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		// Tao list category
		ArrayList<Category> category = new ArrayList<Category>();
		try {
			category.add(new Category(1, "Category1"));
			category.add(new Category(2, "Category2"));
			category.add(new Category(3, "Category3"));
			category.add(new Category(4, "Category4"));
			category.add(new Category(5, "Category5"));
			category.add(new Category(6, "Category6"));
			category.add(new Category(7, "Category7"));
			category.add(new Category(8, "Category8"));
			category.add(new Category(9, "Category9"));
			category.add(new Category(10, "Category10"));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		// Tao list accessory
		ArrayList<Accessory> accessory = new ArrayList<Accessory>();
		try {
			accessory.add(new Accessory(1, "Accessory1"));
			accessory.add(new Accessory(2, "Accessory2"));
			accessory.add(new Accessory(3, "Accessory3"));
			accessory.add(new Accessory(4, "Accessory4"));
			accessory.add(new Accessory(5, "Accessory5"));
			accessory.add(new Accessory(6, "Accessory6"));
			accessory.add(new Accessory(7, "Accessory7"));
			accessory.add(new Accessory(8, "Accessory8"));
			accessory.add(new Accessory(9, "Accessory9"));
			accessory.add(new Accessory(10, "Accessory10"));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		dataBase.insertTable("product", product);
		dataBase.insertTable("category", category);
		dataBase.insertTable("accessory", accessory);

	}

	private static void printTableTest() {
		System.out.println("Print Table : ");
		ArrayList<Object> accessorys = dataBase.getAccessoryTable();
		ArrayList<Object> products = dataBase.getProductTable();
		ArrayList<Object> categorys = dataBase.getCategoryTable();
		System.out.println(products);
		System.out.println(categorys);
		System.out.println(accessorys);
	}
}
