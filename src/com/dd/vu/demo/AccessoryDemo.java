package com.dd.vu.demo;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.entity.Accessory;
import com.dd.vu.entity.Category;

public class AccessoryDemo {
	
	
	public static List<Accessory> createAccessoryTest(){
		List<Accessory> accessory = new ArrayList<Accessory>();
		try {
			accessory.add(new Accessory(1,"Giat Khat"));
			accessory.add(new Accessory(2,"Loai co ga"));
			accessory.add(new Accessory(3,"Loai Sua"));
			accessory.add(new Accessory(4,"Loai co con"));
			return accessory;
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return accessory;
	}
	
	private static Category printCategory(List<Category> lisCategory, int id) {
		Category result = null;
		for (Category temp : lisCategory) {
			if (id == temp.getId()) {
				result = temp;
			}
		}
		return result;
	}

}
