import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Comparator;


public class Lambda {
	@FunctionalInterface
	interface Cal{
		int add(int a , int b);
	}
	// Không có tham số
	interface Test0thamso{
		String say();
	}
	// Có 1 tham số là String
	interface testString{
		String say2(String name);
	}
	
	
	public static void main(String []args) {
		// Biển thức + và -
		Cal cal = (int a, int b) -> (a + b);
        System.out.println(cal.add(3, 5));
        Cal cal2 = (int a,int b) ->(a*b);
        System.out.println(cal2.add(3, 5));
        Cal cal3 = (a,b) -> (a-b);
        System.out.println(cal3.add(10, 5));
        
        // Show list
        List<String> showlist = new ArrayList();
    	showlist.add("1");
    	showlist.add("2");
    	showlist.forEach(p->{
    		System.out.println(p);
    	});
    	
    	//Sắp xếp tên
    	
    	Employee[] employees = {
    			new Employee("A"),
    			new Employee("D"),
    			new Employee ("C"),
    			new Employee ("B")
    	};
    	System.out.println("Before Sorting Name : "+Arrays.toString(employees));
    	Arrays.sort(employees, Employee::nameCompare);
    	System.out.println("Alter Sorting Name : "+Arrays.toString(employees));
    	
    	//Biểu thức Lamba không có tham số
    	
    	Test0thamso s = () -> {
    		return "Không có tham số ";
    	};
    	System.out.println(s.say());
    	
    	// Biểu thức chứa 1 tham số string
    	testString teststring = name -> "Hello " + name;
    	System.out.println(teststring.say2("Trong Vu"));
    	
    	//Biểu thức Lambda tạo Thread
    	// Using an anonymous inner class
    	Runnable r1 = new Runnable() {
            public void run() {
                System.out.println("Runnable 1");
            }
        };
        // Using Lambda Expression for Funcational Interface
        Runnable r2 = () -> System.out.println("Runnable 2");
 
        r1.run();
        r2.run(); 	
	}	
}
