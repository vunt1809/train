import java.util.ArrayList;
import java.util.List;

import Moveable.Moveable;

public class DefaultMethod implements Moveable{
	public static void main(String[] args) {
		DefaultMethod tiger = new DefaultMethod();
		tiger.move();
		
		List<DefaultMethod> list = new ArrayList();
		list.add(new DefaultMethod());
		list.add(new DefaultMethod());
		list.add(new DefaultMethod());
		list.forEach((Moveable p)->p.move());
	}
}

