package Moveable;

public interface Moveable {
	default void move() {
		System.out.println("Tiger is moving");
	}
}
