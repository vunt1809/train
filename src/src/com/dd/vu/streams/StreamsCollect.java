package com.dd.vu.streams;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsCollect {
	public static void main(String[] args) {
		List<String> lines = Arrays.asList("Spring","Summer","Fall","Winter");
		
		// L�?c từ không nào kp là Summer without lambda
		List<String> result = getFilterOutput(lines, "Summer");
		for(String temp : result) {
			System.out.println(temp);
		}
		
		// L�?c từ không nào kp là Winter with lambda
		List<String> result2 = lines.stream()                
                .filter(line -> !"Winter".equals(line))     
                .collect(Collectors.toList());              
		System.out.println("With Lambda : ");
        result2.forEach(System.out::println);
	}
	
	// Method l�?c
	private static List<String> getFilterOutput(List<String> lines, String filter) {
        List<String> result = new ArrayList<>();
        for (String line : lines) {
            if (!"Summer".equals(line)) {
                result.add(line);
            }
        }
        return result;
    }
	
}
