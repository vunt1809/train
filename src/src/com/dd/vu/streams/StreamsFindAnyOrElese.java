package com.dd.vu.streams;

import java.util.Arrays;
import java.util.List;

public class StreamsFindAnyOrElese {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Person> persons = Arrays.asList(
				new Person("Vu",19),
				new Person("Tan",22),
				new Person("Trieu",21),
				new Person("Linh",20),
				new Person("Vu",30)
				
				);
		// Find Name without lambda
		Person result = getStudentByName(persons, "Vu");
		System.out.println(result.getName()+" "+result.getAge());
		
		
		// Find Name with lambda , streams
		Person result1 = persons.stream()
			.filter(x -> "Vu".equals(x.getName()))
			.findAny()
			.orElse(null);
		System.out.println("result1 : "+result1);
		
		// Find Name kh�ng c�
		Person result2 = persons.stream()
				.filter(x -> "ABC".equals(x.getName()))
				.findAny()
				.orElse(null);
		System.out.println("result2 : "+result2);
		
		// Find Name and Age
		Person result3 = persons.stream()
                .filter((p) -> "Vu".equals(p.getName()) && 19 == p.getAge())
                .findAny()
                .orElse(null);
        System.out.println("result 3 :" + result3.getName()+" "+result3.getAge());

        //Find Name and Age C2
        Person result4 = persons.stream()
                .filter(p -> {
                    if ("Vu".equals(p.getName()) && 19 == p.getAge()) {
                        return true;
                    }
                    return false;
                }).findAny()
                .orElse(null);

        System.out.println("result 4 :" + result4);
		
	}
	// Method Find
	private static Person getStudentByName(List<Person> persons, String name) {

        Person result = null;
        for (Person temp : persons) {
            if (name.equals(temp.getName())) {
                result = temp;
            }
        }
        return result;
    }

}
