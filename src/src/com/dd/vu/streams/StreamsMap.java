package com.dd.vu.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Person> persons = Arrays.asList(
				new Person("Vu",19),
				new Person("Tan",22),
				new Person("Trieu",21),
				new Person("Linh",20),
				new Person("Vu",30)
				);
		
		String name = persons.stream()
                .filter(x -> "jack".equals(x.getName()))
                .map(Person::getName)                        
                .findAny()
                .orElse("");
        System.out.println("name : " + name);

        
        List<String> collect = persons.stream()
                .map(Person::getName)
                .collect(Collectors.toList());

        collect.forEach(System.out::println);
	}

}
