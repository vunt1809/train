package com.dd.vu.product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ListProduct {

	public static void main(String[] args) {
		List<Product> listProducts = getProducts();
		
		
		// Show list without Lambda
//		for(Product products : listProducts) {
//			System.out.println(products.getName()+" "+formatter.format(products.saleDate));
//		}
		// Show list with lambda
		listProducts.forEach(p -> {
			System.out.println(p.getId() + " " + p.getName() + " " + p.getCategoryId() + " "
					+ p.getSaleDate() + " " + p.getQulity() + " " + p.isDelete);
		});

		/// Show Product theo ID = 1
		// Không dùng stream
		Product result1 = filterProductById(listProducts, 1);
		System.out.println(result1.getId() + " " + result1.getName());
		// Dùng stream
		Product result2 = listProducts.stream().filter(product -> 1 == product.getId()).findAny().orElse(null);
		System.out.println("Dùng stream : " + result2.getId() + " " + result2.getName());

		/// Show LIST Product by Quility and IsDelete
		// Không dùng stream
		List<Product> result3 = filterProductByQuilityIsDelete(listProducts,false);
		System.out.println(result3);
		// Dùng stream
		List<Product> result4 = listProducts.stream().filter(product -> product.isDelete == false && product.qulity > 0)
				.collect(Collectors.toList());
		System.out.println("Dùng stream : "+result4);

		/// Show LIST Product có SaleDate lớn hơn ngày hiện tại
		// Không dùng stream		
		List<Product>result5 = filterProductBySaleDate(listProducts);
		System.out.println("rs5 : "+result5);
		// Dùng stream		
		List<Product> result6 = listProducts.stream().filter(product -> product.isDelete == false && product.saleDate.isAfter(LocalDate.now())).collect(Collectors.toList());
		System.out.println("Dùng stream : "+result6);
		
		
		///Show tổng số quility chưa bị xóa
		//Không dùng stream
		int result7 = totalProduct(listProducts);
		System.out.println(result7);
		//Dùng stream
		List<Product> result8 = listProducts.stream().filter(product -> product.isDelete == false).collect(Collectors.toList());
		int resultReduce =result8.stream().reduce(0 ,(a,b) ->a + b.getQulity() ,Integer::sum);
		System.out.println("Dùng stream rs 8 : "+result8 +" Số quility : "+resultReduce);
		
		/// Kiểm tra sản phẩn thuộc category
		//Không dùng stream
		Boolean result9 = isHaveProductInCategory(listProducts, 1);
		System.out.println(result9);
		// Dùng stream
//		Boolean result8 = listProducts.stream().filter(product ->1==product.categoryId).findAny().orElse(false);
//		System.out.println(result8);
		
		/// Show list có sale > ngày hiện tại và quility >0
		//Không dùng stream
		List<Product> result11 = filterProductBySaleDateAndQuitily(listProducts);
		System.out.println("rs11 : "+result11);
		// Dùng stream
		List<Product> result12 = listProducts.stream().filter(product -> product.qulity>0 && product.saleDate.isAfter(LocalDate.now())).collect(Collectors.toList());
		System.out.println("Dùng stream rs12 : "+result12);

	}

	// Find Product by ID
	private static Product filterProductById(List<Product> listProducts, int id) {
		Product result = null;
		for (Product temp : listProducts) {
			if (id == temp.getId()) {
				result = temp;
			}
		}
		return result;
	}

	// Find List Product by Quility and IsDelete;
	private static List<Product> filterProductByQuilityIsDelete(List<Product> listProducts, boolean isDelete) {
		List<Product> result = new ArrayList<Product>();
		for (Product temp : listProducts) {
			if (temp.qulity > 0 && isDelete == temp.isDelete) {
				result.add(temp);
			}
		}
		return result;
	}
	// Find List Product có ngày sale lớn hơn ngày hiện tại
	private static List<Product> filterProductBySaleDate(List<Product> listProducts){
		List<Product> result = new ArrayList<Product>();
		for(Product temp : listProducts) {
			if(temp.saleDate.isAfter(LocalDate.now())&&temp.isDelete==false) {
				result.add(temp);
			}
		}
		return result;
		
	}
	// Tính tổng quitily có sản phẩm chưa bị xóa
	private static Integer totalProduct(List<Product> listProducts) {
		int totalProduct = 0;
		for(Product temp : listProducts) {
			if(temp.isDelete==false) {
				totalProduct = totalProduct + temp.qulity;
			}
		}
		return totalProduct;
		
	}
	
	// Check sản phẩm có thuộc categiory
	private static boolean isHaveProductInCategory(List<Product> listProducts,int categoryID){
		Boolean result = null;
		for(Product temp : listProducts) {
			if(categoryID==temp.categoryId) {
				result = true;
			}
			else{
				result = false;
			}
		}
		return result;
	}
	// Find list sản phẩm có ngày sale > today và quility >0
	
	private static List<Product> filterProductBySaleDateAndQuitily(List<Product> listProducts){
		List<Product> result = new ArrayList<Product>();
		for(Product temp : listProducts) {
			if(temp.saleDate.isAfter(LocalDate.now())&&temp.qulity>0) {
				result.add(temp);
			}
		}
		return result;
	}

	// Tạo danh sách
	private static List<Product> getProducts() {
		List<Product> result = new ArrayList<Product>();
		LocalDate hientai = LocalDate.now();
		LocalDate tuansau = hientai.plus(1, ChronoUnit.WEEKS);
		LocalDate tuantruoc = hientai.minus(1, ChronoUnit.WEEKS);
		try {
			result.add(new Product(1, "Cà phê", 2, hientai, 10, true));
			result.add(new Product(2, "Cà phê đen", 2, tuansau, 4, true));
			result.add(new Product(3, "Cà phê sữa", 2, tuansau, 2, false));
			result.add(new Product(4, "Coca cola", 3, tuansau, 0, true));
			result.add(new Product(5, "Pepsi", 3, hientai, 2, false));
			result.add(new Product(6, "7up", 3, hientai, 3, true));
			result.add(new Product(7, "Bánh ngọt", 4, tuantruoc, 4, false));
			result.add(new Product(8, "Bánh sữa", 4, tuantruoc, 8, true));
			result.add(new Product(9, "Nước lọc", 1, tuantruoc, 0, false));
			result.add(new Product(10, "Sữa", 2, hientai, 0, true));
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return result;
	}

}
