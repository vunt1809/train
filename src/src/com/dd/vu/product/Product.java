package com.dd.vu.product;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Product {
	int id;
	String name;
	public int categoryId;
	public LocalDate saleDate;
	public int qulity;
	public boolean isDelete;
	
	public Product(int id,String name , int categoryId , LocalDate saleDate , int qulity , boolean isDelete) {
		this.id = id;
		this.name = name;
		this.categoryId = categoryId;
		this.saleDate = saleDate;
		this.qulity = qulity;
		this.isDelete = isDelete;
	}
	@Override
	public String toString() {
		return this.name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public LocalDate getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(LocalDate saleDate) {
		this.saleDate = saleDate;
	}
	public int getQulity() {
		return qulity;
	}
	public void setQulity(int qulity) {
		this.qulity = qulity;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	// Tao danh sach Product
	public static List<Product> getProducts() {
		List<Product> result = new ArrayList<Product>();
		LocalDate hientai = LocalDate.now();
		LocalDate tuansau = hientai.plus(1, ChronoUnit.WEEKS);
		LocalDate tuantruoc = hientai.minus(1, ChronoUnit.WEEKS);
		try {
			result.add(new Product(1, "Ca  phe", 2, hientai, 10, true));
			result.add(new Product(2, "Ca  phe den", 2, tuansau, 4, true));
			result.add(new Product(3, "Ca  phe sua", 2, tuansau, 2, false));
			result.add(new Product(4, "Coca cola", 3, tuansau, 0, true));
			result.add(new Product(5, "Pepsi", 3, hientai, 2, false));
			result.add(new Product(6, "7up", 3, hientai, 3, true));
			result.add(new Product(7, "Banh ngot", 4, tuantruoc, 4, false));
			result.add(new Product(8, "Banh kem", 4, tuantruoc, 8, true));
			result.add(new Product(9, "Tra sua", 1, tuantruoc, 0, false));
			result.add(new Product(10, "Kem", 2, hientai, 0, true));
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return result;
	}
	
	
}
