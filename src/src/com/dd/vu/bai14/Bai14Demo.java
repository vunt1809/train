package com.dd.vu.bai14;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai14Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Product> listProducts = Product.getProducts();
		
		Integer totalQuility = Bai14.totalProduct(listProducts);
		System.out.println(totalQuility);
		
		Integer totalQuility2 = Bai14Stream.totalProduct(listProducts);
		System.out.println("Stream : "+totalQuility2);
	}

}
