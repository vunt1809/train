package com.dd.vu.bai14;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai14 {
	public static Integer totalProduct(List<Product> listProducts) {
		int totalProduct = 0;
		for(Product product : listProducts) {
			if(product.isDelete==false) {
				totalProduct = totalProduct + product.qulity;
			}
		}
		return totalProduct;
	}

}
