package com.dd.vu.lambda;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LambdaExpression {
	
	private static List<Developer> getDevelopers(){
		List<Developer> result = new ArrayList<Developer>();
		result.add(new Developer("Tan", new BigDecimal("1001"),22));
		result.add(new Developer("Trieu",new BigDecimal("1002"),21));
		result.add(new Developer("Vu",new BigDecimal("1000"),19));
		result.add(new Developer("Linh",new BigDecimal("1003"),20));		
		return result;
	}
	
	public static void main(String[] args) {
		List<Developer> listDevs = getDevelopers();
		
		System.out.println("Before Sort : ");
		for(Developer developers : listDevs) {
			System.out.println(developers.name+" "+developers.salary+" "+developers.age);
		}
		
		// sort by age without lambda
		Collections.sort(listDevs, new Comparator<Developer>() {

			@Override
			public int compare(Developer dev1, Developer dev2) {
				return dev1.getAge()-dev2.getAge();
			}
		});
		
		System.out.println("Alter Sort By Age :");
		for(Developer developers: listDevs) {
			System.out.println(developers.name+" "+developers.salary+" "+developers.age);
		}
		
		// sort by name without lambda
		Collections.sort(listDevs, new Comparator<Developer>() {
			@Override
			public int compare(Developer o1, Developer o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		System.out.println("Alter Sort By Name :");
		for(Developer developers: listDevs) {
			System.out.println(developers.name+" "+developers.salary+" "+developers.age);
		}
		
		//sort by salary without lambda
		Collections.sort(listDevs, new Comparator<Developer>() {
			@Override
			public int compare(Developer o1, Developer o2) {
				return o1.getSalary().compareTo(o2.getSalary());
			}
		});
		System.out.println("Alter Sort By Salary :");
		for(Developer developers: listDevs) {
			System.out.println(developers.name+" "+developers.salary+" "+developers.age);
		}
		
		// Sort with Lambda
		// Age
		listDevs.sort((Developer o1,Developer o2)->o1.getAge()-o2.getAge());
		listDevs.forEach((developers)->System.out.println("Sort by Age with Lambda : "+developers.name+" "+developers.salary+" "+developers.age));;
		// Name
		listDevs.sort((Developer o1,Developer o2)->o1.getName().compareTo(o2.getName()));
		listDevs.forEach((developers)->System.out.println("Sort by Name with Lambda : "+developers.name+" "+developers.salary+" "+developers.age));;
		// Salary 
		listDevs.sort((Developer o1, Developer o2)->o1.getSalary().compareTo(o2.getSalary()));
		listDevs.forEach((developers)->System.out.println("Sort by Salary with Lambda : "+developers.name+" "+developers.salary+" "+developers.age));;	
	}
}
