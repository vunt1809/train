package com.dd.vu.bai15;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai15 {
	public static boolean isHaveProductInCategory(List<Product> listProducts,int categoryId){
		Boolean checkCategoryId = null;
		for(Product product : listProducts) {
			if(categoryId==product.getCategoryId()) {
				checkCategoryId = true;
				return checkCategoryId;
			}
			else{
				checkCategoryId = false;
			}
			
		}
		return checkCategoryId;
	}
}
