package com.dd.vu.bai15;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai15Demo {
	public static void main(String[] args) {
		List<Product> listProducts = Product.getProducts();
		
		boolean checkCategoryId = Bai15.isHaveProductInCategory(listProducts,4);
		System.out.println(checkCategoryId);
		
		boolean checkCategoryId2 = Bai15Stream.isHaveProductInCategory(listProducts, 10);
		System.out.println("Steam : "+checkCategoryId2);
	}
	
	
	
}
