package com.dd.vu.bai12;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai12Demo {
	public static void main(String args[]) {
		List<Product> listProducts = Product.getProducts();	
		List<Product> products = Bai12.filterProductByQuilityIsDelete(listProducts, true);
		System.out.println(products);	
		List<Product> products2 = Bai12Stream.filterProductByQuilityIsDelete(listProducts, false);
		System.out.println("Stream : "+products2);
	}
}
