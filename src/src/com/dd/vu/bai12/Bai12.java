package com.dd.vu.bai12;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.product.Product;

public class Bai12 {
	public static List<Product> filterProductByQuilityIsDelete(List<Product> listProducts, boolean isDelete) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProducts) {
			if (product.qulity > 0 && isDelete == product.isDelete) {
				products.add(product);
			}
		}
		return products;
	}

}
