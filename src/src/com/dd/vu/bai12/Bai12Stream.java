package com.dd.vu.bai12;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.dd.vu.product.Product;

public class Bai12Stream {
	public static List<Product> filterProductByQuilityIsDelete(List<Product> listProducts, boolean isDelete) {
		List<Product> products = listProducts.stream().filter(product -> product.isDelete == isDelete && product.qulity > 0)
				.collect(Collectors.toList());
		return products;
	}

}
