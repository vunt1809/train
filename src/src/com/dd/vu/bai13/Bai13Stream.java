package com.dd.vu.bai13;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.dd.vu.product.Product;

public class Bai13Stream {
	public static List<Product> filterProductBySaleDate(List<Product> listProducts) {
		List<Product> products = listProducts.stream()
				.filter(product -> product.isDelete == false && product.saleDate.isAfter(LocalDate.now()))
				.collect(Collectors.toList());
		return products;
	}

}
