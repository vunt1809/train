package com.dd.vu.bai13;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.dd.vu.product.Product;

public class Bai13 {
	public static List<Product> filterProductBySaleDate(List<Product> listProducts){
		List<Product> products = new ArrayList<Product>();
		for(Product product : listProducts) {
			if(product.saleDate.isAfter(LocalDate.now())&&product.isDelete==false) {
				products.add(product);
			}
		}
		return products;
		
	}

}
