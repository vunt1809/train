package com.dd.vu.bai13;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai13Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Product> listProducts = Product.getProducts();
		
		List<Product> products = Bai13.filterProductBySaleDate(listProducts);
		System.out.println(products);
		
		List<Product> products2 = Bai13Stream.filterProductBySaleDate(listProducts);
		System.out.println("Stream : "+products2);

	}

}
