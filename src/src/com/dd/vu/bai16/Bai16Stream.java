package com.dd.vu.bai16;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.HashMap;
import java.util.Map;

import com.dd.vu.product.Product;

public class Bai16Stream {
	public static String filterProductBySaleDateAndQuitily(List<Product> listProducts) {

		String products = listProducts.stream()
				.filter(product -> product.qulity > 0 && product.saleDate.isAfter(LocalDate.now()))
				.map(map -> map.getId() + map.getName()).collect(Collectors.joining(","));
		return products;

	}
}
