package com.dd.vu.bai16;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.dd.vu.product.Product;

public class Bai16 {
	public static List<String> filterProductBySaleDateAndQuitily(List<Product> listProducts) {
		List<String> products = new ArrayList<String>();
		for (Product product : listProducts) {
			if (product.saleDate.isAfter(LocalDate.now()) && product.qulity > 0) {
				products.add(+product.getId()+ " " + product.getName());
			}
		}
		return products;
	}
}
