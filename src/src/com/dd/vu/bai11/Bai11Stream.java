package com.dd.vu.bai11;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai11Stream {
	public static Product filterProductById(List<Product> listProducts, int id) {
		Product products = listProducts.stream().filter(product -> id == product.getId()).findAny().orElse(null);
		return products;
	}

}
