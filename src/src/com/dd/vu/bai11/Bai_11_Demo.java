package com.dd.vu.bai11;

import java.util.List;


import com.dd.vu.product.Product;

public class Bai_11_Demo {
	public static void main(String[] args) {
		List<Product> listProducts = Product.getProducts();
		
		Product result1 = Bai_11.filterProductById(listProducts, 1);
		System.out.println(result1.getId() + " " + result1.getName());
		
		Product result2 = Bai_11_Stream.filterProductById(listProducts, 1);
		System.out.println("Stream : " + result2.getId() + " " + result2.getName());
	}
	
	
	
}
