package com.dd.vu.bai11;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai_11 {
	public static Product filterProductById(List<Product> listProducts, int id) {
		Product result = null;
		for (Product temp : listProducts) {
			if (id == temp.getId()) {
				result = temp;
			}
		}
		return result;
	}
}
