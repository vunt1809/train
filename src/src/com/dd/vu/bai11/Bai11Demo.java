package com.dd.vu.bai11;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai11Demo {
	public static void main(String[] args) {
		List<Product> listProducts = Product.getProducts();

		Product product1 = Bai11.filterProductById(listProducts, 1);
		System.out.println(product1.getId() + " " + product1.getName());

		Product product2 = Bai11Stream.filterProductById(listProducts, 1);
		System.out.println("Stream : " + product2.getId() + " " + product2.getName());
	}

}
