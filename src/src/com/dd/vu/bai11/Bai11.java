package com.dd.vu.bai11;

import java.util.List;

import com.dd.vu.product.Product;

public class Bai11 {
	public static Product filterProductById(List<Product> listProducts, int id) {
		Product products = null;
		for (Product product : listProducts) {
			if (id == product.getId()) {
				products = product;
			}
		}
		return products;
	}
}
