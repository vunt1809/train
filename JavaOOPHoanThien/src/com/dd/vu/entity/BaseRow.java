package com.dd.vu.entity;

public abstract class BaseRow implements IEntity{
	protected int id;
	protected String name;

	protected BaseRow(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	protected BaseRow() {
	}

	@Override
	public String toString() {
		return super.toString();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
