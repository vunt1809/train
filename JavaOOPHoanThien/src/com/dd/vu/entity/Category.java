package com.dd.vu.entity;

public class Category extends BaseRow {

	public Category(int id, String name) {
		super(id, name);
	}
	
	public Category() {
	}

	@Override
	public String toString() {
		return this.id+" "+this.name;
	}
}
