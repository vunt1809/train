	package com.dd.vu.entity;

public class Product extends BaseRow{
	
	public Product(int id, String name) {
		super(id, name);
	}
	
	public Product() {
	}
	
	@Override
	public String toString() {
		return this.id+" "+this.name;
	}
}
