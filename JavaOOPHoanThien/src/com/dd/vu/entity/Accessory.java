package com.dd.vu.entity;

public class Accessory extends BaseRow{
	public Accessory(int id, String name) {
		super(id, name);
	}

	public Accessory() {
	}

	@Override
	public String toString() {
		return this.id+" "+this.name;
	}
}
