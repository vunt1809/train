package com.dd.vu.demo;

import java.util.ArrayList;

import com.dd.vu.dao.Database;
import com.dd.vu.entity.Accessory;
import com.dd.vu.entity.BaseRow;
import com.dd.vu.entity.Category;
import com.dd.vu.entity.Product;

public class DatabaseDemo {
	static Database dataBase = Database.getInstance();
	
	public static void main(String[] args) {
		insertTableTest();
		selectTableTest();
		updateTableTest();
		updateTableTest2();
		deleteTableTest();
		truncateTableTest();
		initDatabase();
		printTableTest();
	}

	private static void insertTableTest() {
		Product product = new Product(1," Product1");
		Product product2 = new Product(2," Product2");
		dataBase.insertTable("product", product);
		dataBase.insertTable("product", product2);
		Category category = new Category(1," Category1");
		Category category2 = new Category(2," Category2");
		dataBase.insertTable("category", category);
		dataBase.insertTable("category", category2);
		Accessory accessory = new Accessory(1," Accessory");
		Accessory accessory2 = new Accessory(2," Accessory2");
		dataBase.insertTable("accessory", accessory);
		dataBase.insertTable("accessory", accessory2);
	}

	private static void selectTableTest() {	
		ArrayList<BaseRow> products = dataBase.selectTable("product");
		System.out.println("Select :" + products);
		ArrayList<BaseRow> categorys = dataBase.selectTable("category");
		System.out.println("Select :" + categorys);
		ArrayList<BaseRow> accessorys = dataBase.selectTable("accessory");
		System.out.println("Select :" + accessorys);
	}

	private static void updateTableTest() {
		ArrayList<BaseRow> objCategory = dataBase.selectTable("category");
		BaseRow oldCategory = objCategory.get(1);
		Category newCategory = new Category(3,"UpdateCategory");
		dataBase.updateTable("category", oldCategory,  newCategory);
		System.out.println("Old Category : "+oldCategory+" --> "+"New Category : "+newCategory);
		ArrayList<BaseRow> objProduct = dataBase.selectTable("product");
		BaseRow oldProduct = objProduct.get(1);
		Product newProduct = new Product(3,"UpdateProduct");
		dataBase.updateTable("product", oldProduct,  newProduct);
		System.out.println("Old Product : "+oldProduct+" --> "+"New Product : "+newProduct);
		ArrayList<BaseRow> objAccessory = dataBase.selectTable("accessory");
		BaseRow oldAccessory = objAccessory.get(0);
		Accessory newAccessory = new Accessory(3,"UpdateAccessory");
		dataBase.updateTable("accessory", oldAccessory,  newAccessory);
		System.out.println("Old Accessory : "+oldAccessory+" --> "+"New Accessory : "+newAccessory);	
	}
	
	private static void updateTableTest2() {
		Product product = new Product(9," updateProductById");
		dataBase.updateTable("product", 1, product);
		Category category = new Category(9," updateCategoryById");
		dataBase.updateTable("category", 1, category);
		Accessory accessory = new Accessory(9," updateAccessoryById");
		dataBase.updateTable("accessory", 2, accessory);	
	}

	private static boolean deleteTableTest() {
		ArrayList<BaseRow> objCategory = dataBase.selectTable("category");
		BaseRow category = objCategory.get(0);
		Boolean isDelete = dataBase.deleteTable("category", category);
		System.out.println("Delete : " + category + " Trang Thai : " + isDelete);
		return isDelete;
	}

	private static void truncateTableTest() {
		dataBase.truncateTable("product");
	}

	private static void initDatabase() {
		// Tao list Product
		Accessory accessory = new Accessory(1, "Accessory");
		Accessory accessory2 = new Accessory(2, "Accessory2");
		Accessory accessory3 = new Accessory(3, "Accessory3");
		Accessory accessory4 = new Accessory(4, "Accessory4");
		Accessory accessory5 = new Accessory(5, "Accessory5");
		dataBase.insertTable("accessory", accessory);
		dataBase.insertTable("accessory", accessory2);
		dataBase.insertTable("accessory", accessory3);
		dataBase.insertTable("accessory", accessory4);
		dataBase.insertTable("accessory", accessory5);
		Category category1 = new Category(1,"Giat Khat");
		Category category2 = new Category(2,"Loai co ga");
		Category category3 = new Category(4,"Loai Sua");
		Category category4 = new Category(5,"Bia");
		Category category5 = new Category(6,"Ruou");
		dataBase.insertTable("category", category1);
		dataBase.insertTable("category", category2);
		dataBase.insertTable("category", category3);
		dataBase.insertTable("category", category4);
		dataBase.insertTable("category", category5);
		Product product = new Product(1, "Ca  phe");
		Product product2 = new Product(2, "Ca  phe den");
		Product product3 = new Product(3, "Ca  phe sua");
		Product product4 = new Product(4, "Coca cola");
		Product product5 = new Product(5, "Pepsi");
		dataBase.insertTable("product", product);
		dataBase.insertTable("product", product2);
		dataBase.insertTable("product", product3);
		dataBase.insertTable("product", product4);
		dataBase.insertTable("product", product5);
	}

	private static void printTableTest() {
		System.out.println("Print Table : ");
		ArrayList<BaseRow> products = dataBase.selectTable("product");
		System.out.println(products);
		ArrayList<BaseRow> categorys = dataBase.selectTable("category");
		System.out.println(categorys);
		ArrayList<BaseRow> accessorys = dataBase.selectTable("accessory");
		System.out.println(accessorys);
	}
}
