package com.dd.vu.demo;

import java.util.ArrayList;

import com.dd.vu.dao.AccessoryDAO;
import com.dd.vu.dao.BaseDao;
import com.dd.vu.entity.Accessory;
import com.dd.vu.entity.BaseRow;
import com.dd.vu.entity.Product;

public class AccessoryDaoDemo {
	static AccessoryDAO accessoryDao = new AccessoryDAO();
	
	public static void main(String[] args) {
		insertTest();
		updateTest();
		deleteTest();
		findAllTest();
		findByIdTest();
		findByNameTest();
	}

	private static Integer insertTest() {
		int i;
		Accessory accessory = new Accessory(1, "VuAccessory");
		Accessory accessory2 = new Accessory(2, "VuxVuAccessory");
		Accessory accessory3 = new Accessory(3, "VuzVuAccessory");
		if (accessoryDao.insert(accessory) == 1 && accessoryDao.insert(accessory2) == 1
				&& accessoryDao.insert(accessory3) == 1) {
			i = 1;
			System.out.println("Insert Success");
		} else {
			i=0;
			System.out.println("Insert Fail");
		}
		return i;
	}
	
	private static Integer updateTest() {
		int i;
		ArrayList<BaseRow> listAccessorys = accessoryDao.findAll();
		Accessory oldAccessory = (Accessory) listAccessorys.get(1);
		Accessory newAccessory = new Accessory(4, "VuxUpdateAccessory");
		i = accessoryDao.update(oldAccessory, newAccessory);
		if(i==1) {
			System.out.println("Update Accessory Pass");
		}else {
			i = 0;
			System.out.println("Update Accessory Fail");
		}
		return i;
	}
	
	private static boolean deleteTest() {
		ArrayList<BaseRow> listAccessorys = accessoryDao.findAll();
		Accessory accessory = (Accessory) listAccessorys.get(2);
		boolean check = accessoryDao.delete(accessory);
		System.out.println("Delete : "+accessory+" "+check);
		return check;
	}
	
	private static ArrayList<BaseRow> findAllTest(){
		ArrayList<BaseRow> listAccessorys = accessoryDao.findAll();
		System.out.println(listAccessorys);
		return listAccessorys;
	}
	
	private static void findByIdTest() {
		BaseRow accessory = accessoryDao.findById(4);
		System.out.println(accessory);
	}
	
	private static void findByNameTest() {
		BaseRow accessory = accessoryDao.findByName("VuAccessory");
		System.out.println(accessory);
	}
}
