package com.dd.vu.demo;

import java.util.ArrayList;

import com.dd.vu.dao.ProductDAO;
import com.dd.vu.entity.BaseRow;
import com.dd.vu.entity.Product;

public class ProductDaoDemo {
	static ProductDAO productDAO = new ProductDAO();
	
	public static void main(String[] args) {
		insertProductTest();
		updateProductTest();
		deleteProductTest();
		findAllProductTest();
		findByIdTest();
		findByNameTest();
	}
	
	private static Integer insertProductTest() {
		Product product = new Product(1,"VuProduct");
		Product product2 = new Product(2,"VuxProduct");
		Product product3 = new Product(3,"VuzProduct");
		int check;
		if(productDAO.insert(product)==1&&productDAO.insert(product2)==1&&productDAO.insert(product3)==1) {
			check=1;
			System.out.println("Insert Product Success");
		}else {
			check=0;
			System.out.println("Insert Product Fail");
		}
		return check;
	}
	
	private static Integer updateProductTest() {
		ArrayList<BaseRow> listProducts = productDAO.findAll();
		Product oldProduct = (Product) listProducts.get(1);
		Product newProduct = new Product(4,"Vux4UpdateProduct");
		int i = productDAO.update(oldProduct, newProduct);
		if(i==1) {
			System.out.println("Update Product Pass");
		}else {
			i = 0;
			System.out.println("Update Product Fail");
		}
		return i;
	}
	
	private static boolean deleteProductTest() {
		ArrayList<BaseRow> listProducts = productDAO.findAll();
		Product product = (Product) listProducts.get(2);
		boolean check = productDAO.delete(product);
		System.out.println("Delete : "+product+" "+check);
		return check;
	}
	
	private static void findAllProductTest() {
		ArrayList<BaseRow> listProducts = productDAO.findAll();
		System.out.println(listProducts);
	}
	
	private static void findByIdTest() {
		BaseRow product = productDAO.findById(4);
		System.out.println(product);
	}
	
	public static void findByNameTest() {
		BaseRow product = productDAO.findByName("VuProduct");
		System.out.println(product);
	}
}
