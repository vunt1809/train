package com.dd.vu.demo;

import java.util.ArrayList;

import com.dd.vu.dao.CategoryDAO;
import com.dd.vu.entity.BaseRow;
import com.dd.vu.entity.Category;

public class CategoryDaoDemo {
	static CategoryDAO categoryDao = new CategoryDAO();

	public static void main(String[] args) {
		insertCategoryTest();
		updateCategoryTest();
		deleteCategoryTest();
		fillAllTest();
		fillByIdTest();
		fillByNameTest();
	}

	private static Integer insertCategoryTest() {
		Category category = new Category(1, "VuCategory");
		Category category2 = new Category(2, "Vu2Category");
		Category category3 = new Category(3, "Vu3Category");
		int check;
		if (categoryDao.insert(category) == 1 && categoryDao.insert(category2) == 1
				&& categoryDao.insert(category3) == 1) {
			check = 1;
			System.out.println("Insert Success");
		} else {
			check = 1;
			System.out.println("Insert Fail");
		}
		return check;
	}
	
	private static Integer updateCategoryTest() {
		int check;
		ArrayList<BaseRow> objCategory = categoryDao.findAll();
		Category oldCategory = (Category) objCategory.get(2);
		Category newCategory = new Category(4,"Vux4UpdateCategory");
		int i = categoryDao.update(oldCategory, newCategory);
		if(i==1) {
			check = 1;
			System.out.println("Update Success");
		}else {
			check = 1;
			System.out.println("Update Fail");
		}
		return check;
	}
	
	private static boolean deleteCategoryTest() {
		boolean check;
		ArrayList<BaseRow> objCategory = categoryDao.findAll();
		Category category = (Category) objCategory.get(1);
		check = categoryDao.delete(category);
		System.out.println("Delete : "+category+" "+check);
		return check;
	}
	
	private static ArrayList<BaseRow> fillAllTest() {
		ArrayList<BaseRow> listCategory = categoryDao.findAll();
		System.out.println(listCategory);
		return listCategory;
	}
	
	private static void fillByIdTest() {
		BaseRow category = categoryDao.findById(4);
		System.out.println(category);
	}
	
	private static void fillByNameTest() {
		BaseRow category = categoryDao.findByName("VuCategory");
		System.out.println(category);
	}	
}
