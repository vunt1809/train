package com.dd.vu.demo;

import java.util.ArrayList;

import com.dd.vu.dao.Database;
import com.dd.vu.entity.BaseRow;
import com.dd.vu.entity.Product;

public class ProductDemo {
	static Database dataBase = Database.getInstance();
	public static void main(String[] args) {
		createProductTest();
		selectProductTest();	
		printProduct();
	}
	
	private static void createProductTest() {
		Product product = new Product(1, "Ca  phe");
		Product product2 = new Product(2, "Ca  phe den");
		Product product3 = new Product(3, "Ca  phe sua");
		Product product4 = new Product(4, "Coca cola");
		Product product5 = new Product(5, "Pepsi");
		dataBase.insertTable("product", product);
		dataBase.insertTable("product", product2);
		dataBase.insertTable("product", product3);
		dataBase.insertTable("product", product4);
		dataBase.insertTable("product", product5);
	}
	
	private static void selectProductTest() {
		ArrayList<BaseRow> products = dataBase.selectTable("product");
		System.out.println(products);
	}
	
	private static void printProduct() {
		System.out.println(new Product(1, "Ca  phe"));
	}
}
