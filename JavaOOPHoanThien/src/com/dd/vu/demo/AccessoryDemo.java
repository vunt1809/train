package com.dd.vu.demo;

import java.util.ArrayList;

import com.dd.vu.dao.Database;
import com.dd.vu.entity.Accessory;
import com.dd.vu.entity.BaseRow;

public class AccessoryDemo {
	static Database dataBase = Database.getInstance();

	public static void main(String[] args) {
		createAccessoryTest();
		selectAccessoryTest();
		printAccessory();
	}

	private static void createAccessoryTest() {
		Accessory accessory = new Accessory(1, "Accessory");
		Accessory accessory2 = new Accessory(2, "Accessory2");
		Accessory accessory3 = new Accessory(3, "Accessory3");
		Accessory accessory4 = new Accessory(4, "Accessory4");
		Accessory accessory5 = new Accessory(5, "Accessory5");
		dataBase.insertTable("accessory", accessory);
		dataBase.insertTable("accessory", accessory2);
		dataBase.insertTable("accessory", accessory3);
		dataBase.insertTable("accessory", accessory4);
		dataBase.insertTable("accessory", accessory5);
	}
	
	private static void selectAccessoryTest() {
		ArrayList<BaseRow> accessorys = dataBase.selectTable("accessory");
		System.out.println(accessorys);
	}

	private static void printAccessory() {
		System.out.println(new Accessory(2, "Accessory2"));
	}
}
