package com.dd.vu.dao;

import java.util.ArrayList;

import com.dd.vu.entity.Accessory;
import com.dd.vu.entity.BaseRow;

public abstract class BaseDao implements IDao{
	Database database = Database.getInstance();
	protected String tableName;

	public BaseDao(Database database) {
		super();
	}

	public BaseDao() {
	}

	protected Integer insert(BaseRow row) {
		int i = database.insertTable(tableName,row);
		System.out.println(tableName);
		return i;
	}

	protected Integer update(BaseRow oldRow, BaseRow newRow) {
		int i = database.updateTable(tableName, oldRow, newRow);
		return i;
	}

	protected boolean delete(BaseRow row) {
		boolean check = database.deleteTable(tableName, row);
		return check;
	}

	@Override
	public ArrayList<BaseRow> findAll() {
		ArrayList<BaseRow> listAccessorys = database.selectTable(tableName);
		return listAccessorys;
	}

	@Override
	public BaseRow findById(int id) {
		ArrayList<BaseRow> listBaserows = database.selectTable(tableName);
		BaseRow baserows = listBaserows.stream().filter(baserow -> id == baserow.getId()).findAny().orElse(null);
		return baserows;
	}

	@Override
	public BaseRow findByName(String name) {
		ArrayList<BaseRow> listBaserows = database.selectTable(tableName);
		BaseRow baserows = listBaserows.stream().filter(baserow -> name.equals(baserow.getName())).findAny()
				.orElse(null);
		return baserows;
	}
}
