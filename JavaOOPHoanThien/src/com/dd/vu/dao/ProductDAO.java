package com.dd.vu.dao;

import java.util.ArrayList;

import com.dd.vu.entity.BaseRow;
import com.dd.vu.entity.Product;

public class ProductDAO extends BaseDao {
	
	public ProductDAO() {
		super();
		this.tableName = "product";
	}

	public Integer insert(Product row) {
		return super.insert(row);
	}
	
	public Integer update(Product oldRow, Product newRow) {
		return super.update(oldRow, newRow);
	}
	
	public boolean delete(Product row) {
		return super.delete(row);
	}
	
	@Override
	public ArrayList<BaseRow> findAll() {
		return super.findAll();
	}
	
	@Override
	public Product findById(int id) {
		return (Product) super.findById(id);
	}
	
	@Override
	public Product findByName(String name) {
		return (Product) super.findByName(name);
	}
}
