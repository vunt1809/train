package com.dd.vu.dao;

import java.util.ArrayList;

import com.dd.vu.entity.BaseRow;
import com.dd.vu.entity.Category;

public class CategoryDAO extends BaseDao {	
	public CategoryDAO() {
		super();
		this.tableName="category";
	}
	
	public Integer insert(Category row) {
		return super.insert(row);
	}
	
	public Integer update(Category oldRow, Category newRow) {
		return super.update(oldRow, newRow);
	}
	
	public boolean delete(Category row) {
		return super.delete(row);
	}
	@Override
	public ArrayList<BaseRow> findAll() {
		return super.findAll();
	}
	@Override
	public Category findById(int id) {
		return (Category) super.findById(id);
	}
	@Override
	public Category findByName(String name) {
		return (Category) super.findByName(name);
	}
}
