package com.dd.vu.dao;

import java.util.ArrayList;

import com.dd.vu.entity.BaseRow;

public class Database {
	private ArrayList<BaseRow> products;
	private ArrayList<BaseRow> categories;
	private ArrayList<BaseRow> accessorys;
	private static Database instants;
	String nameTalbe;

	private Database() {
		this.products = new ArrayList<BaseRow>();
		this.categories = new ArrayList<BaseRow>();
		this.accessorys = new ArrayList<BaseRow>();
	}

	public static synchronized Database getInstance() {
		if (instants == null) {
			instants = new Database();
		}
		return instants;
	}

	public ArrayList<BaseRow> getListTable(String name) {
		ArrayList<BaseRow> listTable = new ArrayList<BaseRow>();
		if (name.equals("product")) {
			listTable = this.products;
		}
		if (name.equals("category")) {
			listTable = this.categories;
		}
		if (name.equals("accessory")) {
			listTable = this.accessorys;
		}
		return listTable;
	}
	// Object la product , category or accessory
	public Integer insertTable(String name, BaseRow row) {
		int i;
		getListTable(name).add(row);
		i = 1;
		return i;
	}

	public ArrayList<BaseRow> selectTable(String name) {
		return getListTable(name);
	}

	public Integer updateTable(String name, BaseRow oldRow, BaseRow newRow) {
		int i ;
		ArrayList<BaseRow> objEnitity = selectTable(name);
		Integer index = objEnitity.indexOf(oldRow);
		getListTable(name).set(index, newRow);
		i = 1;
		return i;
	}

	public boolean deleteTable(String name, BaseRow row) {
		boolean check = false;
		check = getListTable(name).remove(row);
		return check;
	}

	public void truncateTable(String name) {
		getListTable(name).clear();
	}

	public Integer updateTable(String name, int id, BaseRow row) {
		int check = 0;
		ArrayList<BaseRow> listTable = selectTable(name);
		BaseRow baseRow = listTable.stream().filter(baseRows -> id == baseRows.getId()).findAny().orElse(null);
		Integer index = listTable.indexOf(baseRow);
		getListTable(name).set(index, row);
		check = 1;
		return check;
	}
}
