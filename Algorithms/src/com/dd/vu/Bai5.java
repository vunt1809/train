package com.dd.vu;

public class Bai5 {
	static int[] arrayInt = { 9, 24, 1, 52, 22, 42, 99, 15, 2 };

	public static void main(String[] args) {
		bubbleSortTest(arrayInt);
		for (int i = 0; i < arrayInt.length; i++) {
			System.out.print(arrayInt[i] + " ");
		}
	}

	public static void bubbleSortTest(int[] arr) {
		int temp = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 1; j < arr.length - i; j++) {
				System.out.println("j -1 :"+arr[j-1]);
				System.out.println("j : "+arr[j]);
				if (arr[j - 1] > arr[j]) {
					temp = arr[j - 1];
					arr[j - 1] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}
}
