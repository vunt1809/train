package com.dd.vu;

public class Bai2 {
	static int arrayInt[] = { 9, 24, 1, 52, 18 };

	public static void main(String[] args) {
		printArray(arrayInt);
		pushToArray(arrayInt, 8);
		deleteArrayByIndex(arrayInt, 1);
		deleteArrayByElement(arrayInt, 18);
		findArrayByElement(arrayInt, 24);
		updateArrayByIndex(arrayInt, 0, 99);
		updateArrayByElement(arrayInt, 24, 99);
	}

	public static void printArray(int[] array) {
		System.out.println("Array Length : " + array.length);
		for (int i = 0; i < array.length; i++) {
			System.out.println(" " + arrayInt[i]);
		}
	}

	public static void pushToArray(int[] array, int element) {
		System.out.println("Push to Array : ");
		int newArray[] = new int[array.length + 1];
		for (int i = 0; i < array.length; i++) {
			newArray[i] = array[i];
		}
		newArray[array.length] = element;
		for (int i = 0; i < newArray.length; i++) {
			System.out.println(" " + newArray[i]);
		}
	}

	public static void deleteArrayByIndex(int[] array, int index) {
		int i, j;
		System.out.println("Delete Index of Array : ");
		int newArray[] = new int[array.length - 1];
		int deleteArray[] = new int[1];
		for (j = i = 0; i < array.length; i++) {
			if (i != index) {
				newArray[j] = newArray[i];
				j++;
			} else {
				deleteArray[0] = array[i];
			}
		}
		for (int c = 0; c < newArray.length; c++) {
			System.out.println(" " + newArray[c]);
		}
		System.out.println("Delete : " + deleteArray[0]);
	}

	public static void deleteArrayByElement(int[] array, int element) {
		int i, j;
		System.out.println("Delete Array : ");
		int newArray[] = new int[array.length - 1];
		int deleteArray[] = new int[1];
		for (j = i = 0; i < array.length; i++) {
			if (array[i] != element) {
				newArray[j] = array[i];
				j++;
			} else {
				deleteArray[0] = array[i];
			}
		}
		for (int c = 0; c < newArray.length; c++) {
			System.out.println(" " + newArray[c]);
		}
		System.out.println("Delete : " + deleteArray[0]);
	}

	public static void findArrayByElement(int[] array, int element) {
		int i;
		int newArray[] = new int[1];
		for (i = 0; i < array.length; i++) {
			if (array[i] == element) {
				newArray[0] = array[i];
			}
		}
		System.out.println("Find Index of Array : " + newArray[0]);
	}

	public static void updateArrayByIndex(int[] array, int index, int element) {
		int i;
		System.out.println("Update Index of Array : ");
		int newArray[] = new int[array.length];
		for (i = 0; i < array.length; i++) {
			if (i == index) {
				newArray[i] = element;
			} else {
				newArray[i] = array[i];
			}
		}
		for (int c = 0; c < newArray.length; c++) {
			System.out.println(" " + newArray[c]);
		}
	}

	public static void updateArrayByElement(int[] array, int oldElement, int newElement) {
		int i;
		System.out.println("Update Index of Array : ");
		int newArray[] = new int[array.length];
		for (i = 0; i < array.length; i++) {
			if (array[i] == oldElement) {
				newArray[i] = newElement;
			} else {
				newArray[i] = array[i];
			}
		}
		for (int c = 0; c < newArray.length; c++) {
			System.out.println(" " + newArray[c]);
		}
	}
}
