package com.dd.vu.bai16;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.bai12.Category;
import com.dd.vu.bai12.Product;

public class Bai16Bubble {
	
	static ArrayList<Product> listProducts = Product.getListProduct();
	static ArrayList<Category> listCategoys = Category.getListCategorys();
	
	public static void main(String[] args) {
		List<Product> listProduct = sortByPrice(listProducts);
		System.out.println(listProduct);
	}
	
	public static List<Product> sortByPrice(List<Product> listProduct) {
		List<Product> products = listProduct;
		for(int i =0;i<listProduct.size();i++) {
			for(int j = 1;j<listProduct.size()-i;j++) {
				Product product1 = products.get(j-1);
				Product product2 = products.get(j);
				if(product1.getPrice()>product2.getPrice()) {
					Product temp = product1;
					products.set(j-1,product2);
					products.set(j, temp);
				}
			}
		}
		return products;
	}
}
