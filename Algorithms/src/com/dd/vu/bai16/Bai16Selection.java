package com.dd.vu.bai16;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.bai12.Category;
import com.dd.vu.bai12.Product;

public class Bai16Selection {
	
	static ArrayList<Product> listProducts = Product.getListProduct();
	
	public static void main(String[] args) {
		List<Product> listProduct = sortByPrice(listProducts);
		System.out.println(listProduct);
	}
	
	public static List<Product> sortByPrice(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int i = 0; i < listProduct.size(); i++) {
			int min = i;
			for (int j = i + 1; j < listProduct.size(); j++) {
				int product1 = listProduct.get(j).getPrice();
				int product2 = listProduct.get(min).getPrice();
				if (product1 < product2) {
					min = j;
				}
			}
			Product temp = listProduct.get(i);
			products.set(i, products.get(min));
			products.set(min, temp);
		}
		return products;
	}
}
