package com.dd.vu;

public class Bai7 {
	static int[] arrayInt = { 9, 24, 1, 52, 22, 42, 99, 15, 2 };

	public static void main(String[] args) {
		selection_sort(arrayInt);
		for (int i = 0; i < arrayInt.length; i++) {
			System.out.print(arrayInt[i] + " ");
		}
	}

	private static void selection_sort(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			int min = i;
			System.out.println(i);
			for (int j = i + 1; j < arr.length; j++) {
				System.out.println(j);
				if (arr[j] < arr[min]) {
					min = j;
				}
			}
			int temp = arr[i];
			arr[i] = arr[min];
			arr[min] = temp;
		}

	}
}
