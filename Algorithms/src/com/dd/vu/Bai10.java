package com.dd.vu;

import java.util.Stack;

public class Bai10 {
	static Stack<String> stackString = new Stack<String>();
	static private int top = 0;

	public static void main(String[] args) {
		stackString.add("Test");
		top++;
		pop();
		pushString(stackString,"Vux");
		pushString(stackString,"Vuz");
		pop();
		pop();
		System.out.println(isEmpty());
		pushString(stackString,"Vuz");
		peek();
	}

	private static void pushString(Stack<String> stackString,String element) {
		if(isFull()==1) {
			System.out.println("Stack Full");
			System.exit(1);
		}
		else if(isFull()==0){
			stackString.push(element);
			top++;
		}
	}

	private static void pop() {
		if (isEmpty() == true) {
			String pop = stackString.pop();
			System.out.println("Pop: " + pop);
		}
	}

	private static void peek() {
		stackString.peek();
		System.out.println("Peek : " + stackString.peek());
	}

	private static boolean isEmpty() {
		boolean check;
		if (stackString.isEmpty() == true) {
			System.out.println("Stack is empty");
			check = false;
		} else {
			check = true;
		}
		return check;
	}

	private static int isFull() {
		int check;
		int sizePop = stackString.size();
		if (top == sizePop) {
			check = 1;
		} else {
			check = 0;
		}
		return check;
	}
}
