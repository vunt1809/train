package com.dd.vu.bai18;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.bai12.Category;
import com.dd.vu.bai12.Product;

public class Bai18Insertion {
	static ArrayList<Product> listProducts = Product.getListProduct();
	static ArrayList<Category> listCategoys = Category.getListCategorys();

	public static void main(String[] args) {
		mapProductByCategory(listProducts, listCategoys);
		List<Product> listProduct = sortByCategoryName(listProducts);
		System.out.println(listProduct);
	}

	public static List<Product> sortByCategoryName(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int i = 1; i < listProduct.size(); i++) {
			Product temp = listProduct.get(i);
			int j = i -1;
			while((j > -1) && (products.get(j).getName().compareTo(temp.getCategoryName())>0)) {
				products.set(j+1,listProduct.get(j));
				j = j -1;
			}
			products.set(j+1, temp);
		}
		return products;
	}
	
	public static List<Product> mapProductByCategory(List<Product> listProduct, List<Category> listCategory) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			for (Category category : listCategory) {
				if (product.getCategoryId() == category.getId()) {
					product.setCategoryName(category.getName());
				}
			}
			products.add(product);
		}
		return products;
	}

}
