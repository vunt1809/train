package com.dd.vu.bai18;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.bai12.Category;
import com.dd.vu.bai12.Product;

public class Bai18Selection {
	static ArrayList<Product> listProducts = Product.getListProduct();
	static ArrayList<Category> listCategoys = Category.getListCategorys();
	
	public static void main(String[] args) {
		mapProductByCategory(listProducts, listCategoys);
		List<Product> listProduct = sortByCategoryName(listProducts);
		System.out.println(listProduct);
	}
	
	public static List<Product> sortByCategoryName(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int i = 0; i < listProduct.size(); i++) {
			int min = i;
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(min).getCategoryName().compareTo(listProduct.get(j).getCategoryName()) > 0) {
					System.out.println();
					min = j;
				}
			}
			Product temp = listProduct.get(i);
			products.set(i, products.get(min));
			products.set(min, temp);
		}
		return products;
	}
	
	public static List<Product> mapProductByCategory(List<Product> listProduct, List<Category> listCategory) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			for (Category category : listCategory) {
				if (product.getCategoryId() == category.getId()) {
					product.setCategoryName(category.getName());
				}
			}
			products.add(product);
		}
		return products;
	}

}
