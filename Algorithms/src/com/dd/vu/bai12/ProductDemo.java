package com.dd.vu.bai12;

import java.util.ArrayList;
import java.util.List;

public class ProductDemo {
	static ArrayList<Product> listProducts = Product.getListProduct();
	static ArrayList<Category> listCategoys = Category.getListCategorys();

	public static void main(String[] args) {
//		List<Product> listMapProductByCategory = mapProductByCategory(listProducts, listCategoys);
//		System.out.println(listMapProductByCategory);

//		Product product = findProduct(listProducts, "CPU");
//		System.out.println(product);

//		List<Product> listProductByCategory = findProductByCategory(listProducts, 1);
//		System.out.println(listProductByCategory);

//		List<Product> listProductByPrice = findProductByPrice(listProducts, 120);
//		System.out.println(listProductByPrice);

//		System.out.println(listMapProductByCategory);

//		List<Product> listProductSortByPrice = sortByPrice(listProducts);
//		System.out.println(listProductSortByPrice);

//		List<Product> listProductSortByName = sortByName(listProducts);
//		System.out.println(listProductSortByName);

//		List<Product> listProductSortByCategoryName = sortByCategoryName(listProducts);
//		System.out.println(listProductSortByCategoryName);

//		double i = calSalary(100, 3);
//		System.out.println(i);

//		double j = calSalaryAll(100, 3);
//		System.out.println(j);

		double i = calSalaryRecursion(100, 4);
		System.out.println(i);

//		Integer month = calMonth(100, 0.2);
//		System.out.println(month);
	}

	public static Product findProduct(ArrayList<Product> listProduct, String name) {
		Product product = new Product();
		for (Product products : listProduct) {
			if (products.getName().equals(name)) {
				product = products;
			}
		}
		return product;
	}

	public static List<Product> findProductByCategory(List<Product> listProduct, int categoryId) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			if (product.getCategoryId() == categoryId) {
				products.add(product);
			}
		}
		return products;
	}

	public static List<Product> findProductByPrice(List<Product> listProduct, int price) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			if (product.getPrice() >= price) {
				products.add(product);
			}
		}
		return products;
	}

	public static List<Product> mapProductByCategory(List<Product> listProduct, List<Category> listCategory) {
		List<Product> products = new ArrayList<Product>();
		for (Product product : listProduct) {
			for (Category category : listCategory) {
				if (product.getCategoryId() == category.getId()) {
					product.setCategoryName(category.getName());
				}
			}
			products.add(product);
		}
		return products;
	}

	public static List<Product> sortByPrice(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int i = 0; i < listProduct.size(); i++) {
			int min = i;
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(j).getPrice() < listProduct.get(min).getPrice()) {
					min = j;
				}
			}
			Product temp = listProduct.get(i);
			products.set(i, products.get(min));
			products.set(min, temp);
		}
		return products;
	}

	public static List<Product> sortByName(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int i = 0; i < listProduct.size(); i++) {
			int min = i;
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(j).getName().length() < listProduct.get(min).getName().length()) {
					min = j;
				}
			}
			Product temp = listProduct.get(i);
			products.set(i, products.get(min));
			products.set(min, temp);
		}
		return products;
	}

	public static List<Product> sortByCategoryName(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int i = 0; i < listProduct.size(); i++) {
			int min = i;
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(min).getCategoryName().compareTo(listProduct.get(j).getCategoryName()) > 0) {
					System.out.println();
					min = j;
				}
			}
			Product temp = listProduct.get(i);
			products.set(i, products.get(min));
			products.set(min, temp);
		}
		return products;
	}

	public static Double calSalary(double salary, int n) {
		if (n < 0) {
			System.out.println("Số năm không đc âm");
		} else {
			for (int i = 1; i <= n; i++) {
				salary = salary + salary * 0.2;
			}
		}
		return salary;
	}

	public static double calSalaryRecursion(double salary, int n) {
		if (n == 0) {
			return salary;
		} else {
			return calSalaryRecursion(salary, n - 1) * 1.2;
		}
	}

	public static double calSalaryAll(double salary, int n) {
		double salaryAll = 0;
		if (n < 0) {
			System.out.println("Số năm không đc âm");
		} else {
			salaryAll = salary;
			for (int i = 1; i <= n; i++) {
				salary = salary + salary * 0.2;
				salaryAll = salaryAll + salary;
			}
		}
		return salaryAll;
	}

	public static Integer calMonth(double tienGui, double rate) {
		int month = 0;
		double tienTK = tienGui + tienGui;
		while (tienGui <= tienTK) {
			month++;
			double tienLai = tienGui * rate;
			tienGui = tienGui + tienLai;
		}
		return month;
	}
}
