package com.dd.vu.bai12;


import java.util.ArrayList;

public class Product {
	private String name;
	private int price;
	private int quality;
	private int categoryId;
	private String categoryName;

	public Product(String name, int price, int quality, int categoryId) {
		super();
		this.name = name;
		this.price = price;
		this.quality = quality;
		this.categoryId = categoryId;
	}
	
	public String toString() {
		return this.name + " " + this.price + " " + this.quality + " " + this.categoryId+" "+this.categoryName;

	}

	public Product() {
	}

	public static ArrayList<Product> getListProduct() {
		ArrayList<Product> listProduct = new ArrayList<Product>();
		listProduct.add(new Product("CPU", 750, 10, 1));
		listProduct.add(new Product("RAM", 50, 24, 2));
		listProduct.add(new Product("HDD", 70, 12, 2));
		listProduct.add(new Product("Main", 400, 32, 1));
		listProduct.add(new Product("Keyboard", 30, 50, 4));
		listProduct.add(new Product("Mouse", 25, 50, 4));
		listProduct.add(new Product("VGA", 60, 35, 3));
		listProduct.add(new Product("Monitor", 120, 28, 2));
		return listProduct;
	}
	
	public String getCategoryName() {
		return categoryName;
	}

	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
}
