package com.dd.vu.bai12;

import java.util.ArrayList;

public class Category {
	private int id;
	private String name;

	public Category(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public static ArrayList<Category> getListCategorys() {
		ArrayList<Category> listCategory = new ArrayList<Category>();
		listCategory.add(new Category(1,"Computer"));
		listCategory.add(new Category(2,"Memory"));
		listCategory.add(new Category(3,"Card"));
		listCategory.add(new Category(4,"Accessory"));
		return listCategory;
	}
}
