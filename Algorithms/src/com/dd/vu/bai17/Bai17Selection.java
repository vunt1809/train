package com.dd.vu.bai17;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.bai12.Product;

public class Bai17Selection {
	static ArrayList<Product> listProducts = Product.getListProduct();
	
	public static void main(String[] args) {
		List<Product> listProduct = sortByName(listProducts);
		System.out.println(listProduct);
	}
	
	public static List<Product> sortByName(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int i = 0; i < listProduct.size(); i++) {
			int min = i;
			for (int j = i + 1; j < listProduct.size(); j++) {
				if (listProduct.get(j).getName().length() < listProduct.get(min).getName().length()) {
					min = j;
				}
			}
			Product temp = listProduct.get(i);
			products.set(i, products.get(min));
			products.set(min, temp);
		}
		return products;
	}
}
