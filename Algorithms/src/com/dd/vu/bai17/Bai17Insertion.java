package com.dd.vu.bai17;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.bai12.Category;
import com.dd.vu.bai12.Product;

public class Bai17Insertion {
	static ArrayList<Product> listProducts = Product.getListProduct();
	static ArrayList<Category> listCategoys = Category.getListCategorys();

	public static void main(String[] args) {
		List<Product> listProduct = sortByName(listProducts);
		System.out.println(listProduct);
	}

	public static List<Product> sortByName(List<Product> listProduct) {
		List<Product> products = listProduct;
		for (int i = 1; i < listProduct.size(); i++) {
			Product temp = listProduct.get(i);
			int j = i -1;
			while((j > -1) && (products.get(j).getName().length()>temp.getName().length())) {
				products.set(j+1,listProduct.get(j));
				j = j -1;
			}
			products.set(j+1, temp);
		}
		return products;
	}
}
