package com.dd.vu.bai17;

import java.util.ArrayList;
import java.util.List;

import com.dd.vu.bai12.Category;
import com.dd.vu.bai12.Product;

public class Bai17Bubble {
	static ArrayList<Product> listProducts = Product.getListProduct();
	static ArrayList<Category> listCategoys = Category.getListCategorys();
	
	public static void main(String[] args) {
		List<Product> listProduct = sortByName(listProducts);
		System.out.println(listProduct);
	}
	
	public static List<Product> sortByName(List<Product> listProduct) {
		List<Product> products = listProduct;
		for(int i =0;i<listProduct.size();i++) {
			for(int j = 1;j<listProduct.size()-i;j++) {
				if(products.get(j-1).getName().length()>products.get(j).getName().length()) {
					Product temp = listProduct.get(j-1);
					products.set(j-1, listProduct.get(j));
					products.set(j, temp);
				}
			}
		}
		return products;
	}

}
