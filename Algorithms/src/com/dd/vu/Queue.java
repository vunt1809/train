package com.dd.vu.bai2122;

import com.dd.vu.bai12.Product;

public class Queue {
	private Object[] queueArray;
	private int top;
	private int min;
	private int maxSize;

	public Queue(int max) {
		maxSize = max;
		queueArray = new Object[maxSize];
		top = -1;
		min = 0;
	}

	public static void main(String[] args){
		Queue queue = new Queue(3);
		queue.push(new Product("Mot", 750, 10, 1));
		queue.push(new Product("Hai", 750, 10, 1));
		queue.push(new Product("Ba", 750, 10, 1));
		queue.get();
		queue.get();
		queue.get();
		queue.get();
		queue.print(queue);
	}

	public void print(Queue queue) {
		for (int i = min; i <= queue.top; i++) {
			System.out.println(queueArray[i]);
		}
	}

	public void push(Object object) {
		try {
			queueArray[++top] = object;
		} catch (Exception e) {
			System.out.println("Mảng đã đầy");
		}
	}

	public Object get() {
		Object obj = new Object();
		try {
			obj = queueArray[min++];
		}catch (ArrayIndexOutOfBoundsException e) {
			// TODO: handle exception
			System.out.println("Mảng trống");
		}
		return obj;
	}
}
