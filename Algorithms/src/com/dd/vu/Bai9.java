package com.dd.vu;

public class Bai9 {
	public static void main(String[] args) {
		fibonacci();
	}

	public static void fibonacci() {
		int i1 = 0;
		int i2 = 1;
		int i3;
		for (int i = 0; i < 5; i++) {
			i3 = i1 + i2;
			System.out.println(" " + i3);
			i1 = i2;
			i2 = i3;
		}
	}
}
